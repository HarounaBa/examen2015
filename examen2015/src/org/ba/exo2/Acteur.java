package org.ba.exo2;

public class Acteur {
	private String nom;
	private String prenom;

	//Constructeurs
	public Acteur(){
		this.nom = "";
		this.prenom= "";
	}

	public Acteur(String nom, String prenom){
		this.nom = nom;
		this.prenom=prenom;
	}


	public String getNom() {
		return nom;
	}


	public String getPrenom() {
		return prenom;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Acteur other = (Acteur) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "" + nom + "," +prenom + "";
	}

	public  int compareTo(Acteur acteur) {


		if (getNom().equals(acteur.getNom())) {
			return getPrenom().compareTo(acteur.getPrenom()) ;
		}  else {
			return getNom().compareTo(acteur.getNom()) ;
		}
	}


}
