package org.ba.exo2;

public class Film {
	private String titre;
	private int anneeRealisation;
	
	
	public Film(String titre, int anneeRealisation) {
		this.titre = titre;
		this.anneeRealisation = anneeRealisation;
	}
	
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public int getAnneeRealisation() {
		return anneeRealisation;
	}
	public void setAnneeRealisation(int anneeRealisation) {
		this.anneeRealisation = anneeRealisation;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anneeRealisation;
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Film other = (Film) obj;
		if (anneeRealisation != other.anneeRealisation)
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return   titre + ", "+ anneeRealisation + "\n";
	}
	
}
