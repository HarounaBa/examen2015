package org.ba.exo2;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class MainExercice2 {

	public static void main(String[] args) {
		NavigableMap<String, Film>map = new TreeMap<String, Film>();
		// TODO Auto-generated method stub
		Film film1 = new Film("Les Tontons Flingueurs", 1963);
		Film film2 = new Film("Les Barbouzes", 1964);
		Film film3 = new Film("Ne nous f�chons pas", 1966);
		Realisateur realisateur = new Realisateur("Lautner","Georges");
		
		realisateur.addFilm(film1);
		realisateur.addFilm(film2);
		realisateur.addFilm(film3);
		
		System.out.println(realisateur);
		
		realisateur.removeFilm(film1);
		System.out.println("Nombre de films:" + realisateur.getNombreFilms());
		
	
		//for(Map.Entry<String, Film> entry : map.entrySet())
			//System.out.println("[" + entry.getKey() + "]" + entry.getValue());
	}

}
