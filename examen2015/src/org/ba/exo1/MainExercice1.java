package org.ba.exo1;

import java.util.SortedSet;
import java.util.TreeSet;

public class MainExercice1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			SortedSet<Acteur> set =  new TreeSet<Acteur>() ;
			Acteur a1 = new Acteur("Ba", "Rona");
			System.out.println("Acteur : " + a1);
			
			Acteur a2 = new Acteur("Ndiaye", "Alou");
			Acteur a3 = new Acteur("Ba", "Mamadou");
			
			set.add(a1) ;
			set.add(a2) ;
			set.add(a3);
			
			 for (Acteur a : set) {
				   System.out.println(a) ;
				}
		}
	}

