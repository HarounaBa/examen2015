package org.ba.exo3;

import java.util.ArrayList;

import org.ba.exo2.Film;

public class Realisateur {
	private String nom;
	private String prenom;
	ArrayList<Film> listeFilms;

	public Realisateur(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
		listeFilms = new ArrayList<Film>();
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/*addFilm de la question 6 */
	public boolean addFilm(Film film){
		listeFilms.add(film);
		return true;
	}

	/*removeFilm de la question 6 */
	public boolean removeFilm(Film film){

		if(listeFilms.remove(film) == true) 
			return true;
		else 
			return false;
	}

	/*getNombreFilms de la question 6 */
	public int getNombreFilms(){
		return listeFilms.size();
	}

	//	/*addFilm de la question 11 */
	//	public boolean addFilm(Film film){
	//		NavigableMap<String, Film>map = new TreeMap<String, Film>();
	//		map.put(film.getTitre(), film);
	//		return true;
	//	}

	//	/*removeFilm de la question 11 */
	//	public boolean removeFilm(Film film){
	//		NavigableMap<String, Film>map = new TreeMap<String, Film>();
	//		map.remove(film.getTitre());
	//			return true;
	//	}
	//	/*getNombreFilms de la question 11 */
	//	public int getNombreFilms(){
	//		NavigableMap<String, Film>map = new TreeMap<String, Film>();
	//		
	//		return map.size();
	//	}

	@Override
	public String toString() {
		return "Realisateur : " + prenom + " " + nom
				+ "\n"  ;
	}

	//	public Film getFilm(int annee){
	//		
	//	}
}
