package org.ba.exo3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class LisEcrisRealisateur {
	private Writer writer;
	
	public LisEcrisRealisateur(Writer writer){
		this.writer = writer;
		}
	
	public  void sauveRealisateur(Realisateur realisateur){
		try{
			File fichier = new File("fichier.txt");
			Writer writer = new FileWriter(fichier, true);
			writer.write(realisateur.getNom());
			writer.write("|");
			writer.write(realisateur.getPrenom());
			writer.write("|");
			//writer.write(realisateur.listeFilms);
			writer.write("\r\n");
			writer.close();
		}
		catch(IOException e){
			System.out.println("Erreur" + e.getMessage());
			e.printStackTrace();
		}
	}
}
